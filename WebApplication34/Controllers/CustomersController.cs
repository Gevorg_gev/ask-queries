using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication34.Models.DB;

namespace WebApplication34
{
    [ApiController]
    [Route("[controller]")] 
    public class CustomersController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetCustomersCountByCountryAndCity()
        {
            Array customersCountByCountryAndCity;
            using (var db = new NORTHWNDContext())
            {
                customersCountByCountryAndCity = db.Customers
                                                 .GroupBy(x => new { x.Country, x.City })
                                                 .Select(x => new { x.Key.Country, x.Key.City, CustomersCount = x.Count() })
                                                 .ToArray();
            }
            return Ok(customersCountByCountryAndCity);
        }

        [HttpGet]
        public IActionResult GetNoOrderCustomers()
        {
            Array noOrderCustomers;
            using (var db = new NORTHWNDContext())
            {
                noOrderCustomers = db.Customers
                                   .GroupJoin(db.Customers, x => x.CustomerId, y => y.CustomerId, (x, y) => new { x.CustomerId })
                                   .Select(x => x.CustomerId.DefaultIfEmpty())
                                   .ToArray();
            }
            return Ok(noOrderCustomers); 
        }
    }
} 