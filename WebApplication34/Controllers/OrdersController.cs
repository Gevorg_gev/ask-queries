using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication34.Models.DB;

namespace WebApplication34
{
    [ApiController]
    [Route("[controller]")]
    public class OrdersController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetHighFreightCharges()
        {
            Array highFreightCharges;
            using (var db = new NORTHWNDContext())
            {
                highFreightCharges = db.Orders
                                     .GroupBy(x => x.ShipCountry, x => x.Freight)
                                     .Select(x => new { x.Key })
                                     .Take(3)
                                     .ToArray();
            }
            return Ok(highFreightCharges);
        } 

        [HttpGet]
        public IActionResult GetHighFreightChargesIn1996()
        {
            Array highFreightChargesIn1996;
            using (var db = new NORTHWNDContext())
            {
                highFreightChargesIn1996 = db.Orders
                                     .Where(x => x.OrderDate = 1996)
                                     .GroupBy(x => x.ShipCountry, x => x.Year) 
                                     .Select(x => new { x.Key })
                                     .Take(3)
                                     .ToArray();
            }
            return Ok(highFreightChargesIn1996);
        }
    }
}