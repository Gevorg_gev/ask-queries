using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication34.Models.DB;

namespace WebApplication34
{
    [ApiController]
    [Route("[controller]")] 
    public class ProductsController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetProductsCountByCategory()
        {
            Array productsCountByCategory;
            using (var db = new NORTHWNDContext())
            {
                productsCountByCategory = db.Products
                                          .Join(db.Categories, x => x.CategoryId, y => y.CategoryId, (x, y) => new { y.CategoryName })
                                          .GroupBy(x => x.CategoryName)
                                          .Select(x => new { x.Key, ProductCount = x.Count() })
                                          .OrderByDescending(x => x.ProductCount)
                                          .ToArray();

                return Ok(productsCountByCategory);
            }
        }

        [HttpGet]
        public IActionResult GetReorderedProducts() 
        {
            Array reorderedProducts;
            using (var db = new NORTHWNDContext())
            {
                reorderedProducts = db.Products
                                    .Where(x => x.UnitsInStock < x.ReorderLevel)
                                    .GroupBy(x => new { x.UnitsInStock, x.ReorderLevel, x.ProductId, x.ProductName })
                                    .Select(x => new { x.Key.ProductId })
                                    .OrderBy(x => x.ProductId)
                                    .ToArray();
            }
            return Ok(reorderedProducts);
        }

        [HttpGet]
        public IActionResult GetReorderedProductsContinue()
        {
            Array reorderedProductsContinue;
            using (var db = new NORTHWNDContext())
            {
                reorderedProductsContinue = db.Products
                                            .Where(x => x.UnitsInStock + x.UnitsOnOrder <= x.ReorderLevel && x.Discontinued == false)
                                            .GroupBy(x => new { x.UnitsInStock, x.UnitsOnOrder, x.ReorderLevel, x.Discontinued })
                                            .Select(x => new { x.Key.UnitsInStock, x.Key.UnitsOnOrder })
                                            .ToArray();
            }
            return Ok(reorderedProductsContinue);
        }
    }
}