﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication34.Models.DB
{
    public partial class ProductSalesFor1997
    {
        public string CategoryName { get; set; }
        public string ProductName { get; set; }
        public decimal? ProductSales { get; set; }
    }
}
