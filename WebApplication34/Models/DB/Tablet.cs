﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication34.Models.DB
{
    public partial class Tablet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string Country { get; set; }
    }
}
